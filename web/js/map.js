var obj = JSON.parse(data);
var ant = JSON.parse(antenna);

var mymap = L.map('mapid').setView([45.7773305, 3.0979324], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
	maxZoom: 18,
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
		'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	id: 'mapbox.streets'
}).addTo(mymap);

L.control.scale().addTo(mymap);

var LeafIcon = L.Icon.extend({
	options: {
	    shadowUrl: 'js/images/marker-shadow.png',

	    iconSize:     [25, 41], // size of the icon
	    shadowSize:   [41, 41], // size of the shadow
	    iconAnchor:   [11, 41], // point of the icon which will correspond to marker's location
	    shadowAnchor: [11, 41],  // the same for the shadow
	    popupAnchor:  [3, -35] // point from which the popup should open relative to the iconAnchor
	}
});

var redMarker = new LeafIcon({iconUrl: 'js/images/redMarker.png'}),
	greenMarker = new LeafIcon({iconUrl: 'js/images/greenMarker.png'}),
	pinkMarker = new LeafIcon({iconUrl: 'js/images/pinkMarker.png'}),
	orangeMarker = new LeafIcon({iconUrl: 'js/images/orangeMarker.png'});

for(i in obj){
	if(obj[i].positionned){
		var marker = L.marker([obj[i].lat, obj[i].lng], {icon: pinkMarker}).addTo(mymap).bindPopup("<b>Antenna " + obj[i].stationId +" </b><br /> Radius: " + obj[i].radius);
	} else if(obj[i].bypassed){
		var marker = L.marker([obj[i].lat, obj[i].lng], {icon: redMarker}).addTo(mymap).bindPopup("<b>Antenna " + obj[i].stationId +" </b><br /> Radius: " + obj[i].radius);
	} else {
		var marker = L.marker([obj[i].lat, obj[i].lng]).addTo(mymap).bindPopup("<b>Antenna " + obj[i].stationId +" </b><br /> Radius: " + obj[i].radius);
	}
}

for(i in ant){
	if(ant[i].bypassed){
		var marker = L.marker([ant[i].lat, ant[i].lng], {icon: orangeMarker}).addTo(mymap).bindPopup("<b>Antenna " + ant[i].stationId +" </b><br /> Radius: " + ant[i].radius);
	} else {
		var marker = L.marker([ant[i].lat, ant[i].lng], {icon: greenMarker}).addTo(mymap).bindPopup("<b>Antenna " + ant[i].stationId +" </b><br /> Radius: " + ant[i].radius);
	}
}

var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
        grades = ["Old Position", "Positionned Antenna", "Old byPassed Position", "New Position", "New byPassed Position"],
        labels = ["js/images/marker-icon.png","js/images/pinkMarker.png","js/images/redMarker.png","js/images/greenMarker.png","js/images/orangeMarker.png"];

    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            grades[i] + (" <img src="+ labels[i] +" height='30' width='20'>") +'<br>';
    }

    return div;
};

legend.addTo(mymap);
package sigfox;

import java.awt.Desktop;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
	
    /**
     * @param args the command line arguments
     */ 
	public static void main(String[] args){
		//antennaReverseLocation("0DD4", 500, true);
        areaReverseLocation(45.7, 3, 45.9, 3.2);
		//deviceLocation("20402E", 100);
	}
	
	public static void antennaReverseLocation(String StationId, int size) {
		antennaReverseLocation(StationId, size, false);
	}
	
	public static void antennaReverseLocation(String StationId, int size, boolean debug) {
		String JsonData = new String();
		
		/* Taking the StationId directly from the json record */
        SigStation Station = new SigStation(StationId);
		
        String Bearer = CurlRequest.APIConnection("projet.isima.2018", "ProjetIsima2018!");
        JsonData = CurlRequest.GetJsonFromStationId(Bearer, StationId);
        JsonDecode.Decode(JsonData, Station);
        
        /* Station de reference pour connaitre l'erreur de position de notre algorithme */
        SigStationPoint station = new SigStationPoint(Station.getLat(), Station.getLng(), 0, 0); 
        System.out.println("===== Resultats =====");
        System.out.println("lat\t\tlng\t   \terr(m)\tMethod");
        System.out.println(Station.getLat() + "\t" + Station.getLng() + "\t" + "0000" + "\tOriginal ");            
       
        /* Methode du tableau avec maille de 100m */
        SigStationTab t = new SigStationTab(Station, size);
        SigStationPoint avg = t.getAntennaPosition();
        System.out.format("%.7f\t%.7f\t%d\tArea Density " + size + "m\n", avg.getLat(), avg.getLng(), station.getDistanceInMeters(avg));
        
        if(debug) {
	        try {
	            
		        BufferedWriter writer = new BufferedWriter(new FileWriter("web/json/antenna.json"));
				writer.write("data = '");
				writer.write(JsonData);
				writer.write("';");
				writer.close();
			
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
	        
	        try {
	        
		        BufferedWriter wr = new BufferedWriter(new FileWriter("web/json/new.json"));
				wr.write("ant = '");
				wr.write(t.getAntennaPositionInJSON().toString());
				wr.write("';");
				wr.close();	
				
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
			
			t = null;
			
			File htmlFile = new File("web/antenna.html");
			
			try {
				Desktop.getDesktop().browse(htmlFile.toURI());
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
	}
       
	public static void areaReverseLocation(double minLat, double minLng, double maxLat, double maxLng) {
		String JsonData = new String();
		List<String> list = new ArrayList<String>();
		
		String Bearer = CurlRequest.APIConnection("projet.isima.2018", "ProjetIsima2018!");
		JsonData = CurlRequest.GetJsonFromArea(Bearer, minLat, minLng, maxLat, maxLng);
		
		try {
			
			BufferedWriter writer = new BufferedWriter(new FileWriter("web/json/oldAntenna.json"));
			writer.write("data = '");
			writer.write(JsonData);
			writer.write("';");
			writer.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			
			BufferedWriter wr = new BufferedWriter(new FileWriter("web/json/newAntenna.json"));
			wr.write("antenna = '[");
		
			JsonDecode.DecodeArray(JsonData, list);
			
			for(int i = 0; i < list.size(); ++i) {
				String s = list.get(i);
				SigStation Station = new SigStation(s);
				String Json = CurlRequest.GetJsonFromStationId(Bearer, s);
		        JsonDecode.Decode(Json, Station);
		        
		        SigStationTab t = new SigStationTab(Station, 100);
		        wr.write(t.getAntennaPositionInJSON().toString());
		        if(i != (list.size() -1))
		        	wr.write(",");
		        
		        t = null;
			}
			
			wr.write("]';");
			wr.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		File htmlFile = new File("web/map.html");
		
		try {
			Desktop.getDesktop().browse(htmlFile.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void deviceLocation(String deviceId, int size) {
		String JsonData;
		String comId;
		String Bearer = CurlRequest.APIConnection("projet.isima.2018", "ProjetIsima2018!");
		SigStation device = new SigStation(deviceId);
		List<String> stations = new ArrayList<String>();
		List<Double> rssis = new ArrayList<Double>();
		
		JsonData = CurlRequest.GetLastCommunicationFronDeviceId(Bearer, deviceId);
		comId = JsonDecode.getComId(JsonData);
		JsonData = null;
		
		JsonData = CurlRequest.GetJsonFromCommunicationId(Bearer, deviceId, comId);
		JsonDecode.decodeCommunicationStationList(JsonData, stations, rssis);
		
		try {
			
			BufferedWriter writer = new BufferedWriter(new FileWriter("web/json/communication.json"));
			writer.write("data = '");
			writer.write(JsonData);
			writer.write("';");
			writer.close();
		
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		JsonData = null;
		
		
		
		try {
            
	        BufferedWriter wr = new BufferedWriter(new FileWriter("web/json/station.json"));
			wr.write("station = '[");
			for(int i = 0; i < stations.size(); ++i) {
				JsonData = CurlRequest.GetJsonFromStationId(Bearer, stations.get(i));
				JsonDecode.decodeStationCommunicationLocation(JsonData, device, rssis.get(i));
				wr.write(JsonData);
				if(i < stations.size()-1)
					wr.write(",");
			}
			wr.write("]';");
			wr.close();	
			
        } catch (IOException e) {
        	e.printStackTrace();
        }
		
		SigStationTab t = new SigStationTab(device, size);
		SigStationPoint avg = t.getAntennaPosition();
        System.out.format("%.7f\t%.7f\tArea Density " + size + "m\n", avg.getLat(), avg.getLng());
        
        try {
            
	        BufferedWriter wr = new BufferedWriter(new FileWriter("web/json/position.json"));
			wr.write("dev = '");
			wr.write(t.getAntennaPositionInJSON().toString());
			wr.write("';");
			wr.close();	
			
        } catch (IOException e) {
        	e.printStackTrace();
        }
        
        File htmlFile = new File("web/device.html");
		
		try {
			Desktop.getDesktop().browse(htmlFile.toURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
        
	}

}
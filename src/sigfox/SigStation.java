package sigfox;

import java.util.ArrayList;

/**
 * SigStation is a class representing an antenna an all the communication with it
 * @author Sarah Hafsi, Thibaut Loiseau
 * @see SigStationPoint
 */
public class SigStation {
	
	/**
	 * Sigfox Station ID
	 */
    private String stationId;
    
    
    /**
     * Estimated latitude of the antenna
     */
    private double lat;
    
    
    /**
     * Estimated longitude of the antenna
     */
    private double lng;
    
    
    /**
     * Estimated radius error of the antenna location
     */
    private double radius;
    
    
    /**
     * List of captured points
     */
    protected ArrayList<SigStationPoint> points;

    
    /**
     * Constructs a SigStation with an ID, a location an a list of communications captured
     * @param stationId Sigfox station ID
     * @param lat Latitude of the antenna
     * @param lng Longitude of the antenna
     * @param radius Radius error of the antenna location
     * @param points List of captured points
     */
    public SigStation(String stationId, double lat, double lng, double radius, ArrayList<SigStationPoint> points){
        this.stationId = stationId;
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
        if(points != null)
            this.points = points;
        else
            this.points = new ArrayList<>();
    }

    
    /**
     * Constructs a SigStation only with an station ID
     * @param stationId Sigfox station ID
     */
    public SigStation(String stationId){
        this(stationId, 0, 0, 0, new ArrayList<SigStationPoint>());
    }
    
    
    /**
     * Returns the Sigfox station ID of the antenna
     * @return the Sigfox station ID
     */
	public String getStationId() {
    	return this.stationId;
    }
     
	
	/**
	 * Replaces the current Sigfox station ID with the specified ID.
	 * @param Id Sigfox station ID
	 */
    public void setStationId(String Id) {
    	this.stationId = Id;
    }
    
    
    /**
     * Returns the current latitude of the antenna
     * @return the latitude of the antenna
     */
    public double getLat() {
    	return this.lat;
    }
    
    
    /**
     * Replaces the current latitude of the antenna with the specified latitude
     * @param lat the latitude of the antenna
     */
    public void setLat(double lat) {
    	this.lat = lat;
    }     
    
    
    /**
     * Returns the current longitude of the antenna
     * @return the longitude of the antenna
     */
    public double getLng() {
    	return this.lng;
    }
    
    
    /**
     * Replaces the current longitude of the antenna with the specified longitude
     * @param lat the longitude of the antenna
     */
    public void setLng(double lng) {
    	this.lng = lng;
    }
    
    
    /**
     * Returns the current radius error of the antenna location
     * @return the error radius
     */
    public double getRadius() {
    	return this.radius;
    }
    
    
    /**
     * Replaces the current radius error with the specified radius error
     * @param rad radius error of the antenna location
     */
    public void setRadius(double rad) {
    	this.radius = rad;
    }

}

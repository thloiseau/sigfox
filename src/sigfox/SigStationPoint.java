package sigfox;

import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.log;
import static java.lang.Math.pow;
import static java.lang.Math.round;

/**
 * SigStationPoint is a class which represents a communication point with a Sigfox Station
 * @author Sarah Hafsi, Thibaut Loiseau
 */
public class SigStationPoint {
	
	/**
	 * Point latitude
	 */
    private double lat;
    
    /**
     * Point longitude
     */
    private double lng;
    
    /**
     * Point Radius
     */
    private double radius;
    
    /**
     * Measured RSSI
     */
    private double rssi;
    
    /**
     * Distance effected by the communication
     */
    private double rayon;
    
    final static SigStationPoint origin = new SigStationPoint(0,0,0,0);

    /**
     * Constructs a SigStationPoint with a location, a radius error and a RSSI
     * @param lat Point latitude
     * @param lng Point longitude
     * @param radius Point radius
     * @param rssi Measured RSSI
     */
    public SigStationPoint(double lat, double lng, double radius, double rssi){
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
        this.rssi = rssi;
        this.rayon = Math.round(this.getRadiusInMetersFromRSSI());
    }
    
    
    /**
     * Constructs a SigStationPoint with a lat and lng
     * @param lat Point latitude
     * @param lng Point longitude
     */
    public SigStationPoint(double lat, double lng){
        this.lat = lat;
        this.lng = lng;
    }
    
    /**
     * Returns the current latitude of the point
     * @return the latitude of the point
     */
    public double getLat() {
    	return this.lat;
    }
    
    /**
     * Returns the current longitude of the point
     * @return the longitude of the point
     */
    public double getLng() {
    	return this.lng;
    }
    
    /**
     * Returns the current radius error of the point
     * @return the radius error
     */
    public double getRadius() {
    	return this.radius;
    }
    
    /**
     * Replaces the current radius error with the specified radius
     * @param d new radius error
     */
    public void setRadius(double d) {
    	if(d>=0)
    		this.radius = d;
    }
    
    /**
     * Returns the current measured RSSI
     * @return Measured RSSI
     */
    public double getRSSI() {
    	return this.rssi;
    } 
    
    /**
     * Returns the current effected distance of the communication
     * @return the effected distance of the communication
     */
    public double getRayon() {
    	return this.rayon;
    }  
    
    /**
     * Returns the distance to the emetter using RSSI and FSPL formula
     * @return the distance to the emetter
     */
    public double getRadiusInMetersFromRSSI(){
        return pow(10, (27.55 - (20 * log(868) / log(10)) + abs(this.rssi))/20);
    } 
    
    /**
     * Returns the latitude from equator position
     * @return latitude from the origin
     */
	public int getLatPositionFromOriginInMeters(){
    	return (int)(this.lat * 111195);
    }
    
	/**
	 * Returns the longitude from Greenwich meridian
	 * @return the longitude from the origin
	 */
    public int getLngPositionFromOriginInMeters() {
    	return (int)(this.lng * 111195 * cos(Math.toRadians(this.lat)));
    }
    
    /**
     * Computes the distance between two given SigStationPoints
     * The result is given in meters rounded by getRoundedDouble(). Only works on Earth!
     * @param p : SigStationPoint 
     * @return the distance between the current point and the given one
     */
    public int getDistanceInMeters(SigStationPoint p){
        final int R = 6371; // Radius of the earth
        double distance;
        double latDistance;
        double lonDistance;
        double temp;
        double c;
        
        latDistance = Math.toRadians(p.getLat() - this.lat);
        lonDistance = Math.toRadians(p.getLng() - this.lng);
        temp = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
                + Math.cos(Math.toRadians(this.lat)) * Math.cos(Math.toRadians(p.getLat()))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        c = 2 * Math.atan2(Math.sqrt(temp), Math.sqrt(1 - temp));
        
        distance = R * c * 1000; // convert to meters
        distance = round(Math.sqrt( Math.pow(distance, 2)));  
        
        return (int)distance;
    }
    
}
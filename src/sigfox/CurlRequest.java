package sigfox;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Base64;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.HttpClientBuilder;


/**
 * CurlRequest is a class which allows you to log in API foxtrackr and to get the antenna data with requests
 * @author Sarah Hafsi, Thibaut Loiseau 
 */
public class CurlRequest {
	private enum Method{
		GET, POST;
	}
	
	public static String computeRequest(String url, Method met, String... headers) {
		String jsonData = new String();
		
		try {
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpRequestBase httpRequest = null;
			
			switch(met) {
				case GET :
					httpRequest = new HttpGet(url);
					break;
				case POST :
					httpRequest = new HttpPost(url);
					break;
			}
			
			for(int i = 1; i < headers.length; i+=2) {
				httpRequest.setHeader(headers[i-1], headers[i]);
			}
	
	        HttpResponse response = httpClient.execute(httpRequest);
	        if(response.getStatusLine().getStatusCode() == 200) {
		        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
				String inputLine;
				while((inputLine = rd.readLine())!= null)
					jsonData += inputLine;
	        }

		} catch (IOException e){
			e.printStackTrace();
		}
		
		return jsonData;
	}
	
	
    /**
     * Performs log in Curl request on foxtrackr's api using given user name and password
     * @param username user name
     * @param password password
     * @return Bearer as String 
     */
	public static String APIConnection(String username, String password){
		String urlLogin = "https://api.foxtrackr.com/sign/3.0/in";
		String Token = null;
		String encoding = "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
		
		Token = computeRequest(urlLogin, Method.POST, "Accept", "text/html", "Authorization", encoding, "Content-Type", "application/json").substring(15);
		
        return Token;
	}	
	
	
    /**
     * Performs Curl request on foxtrackr's api using given bearer and station ID
     * @param StationId Sigfox station ID
     * @param Bearer Token
     * @return Parsed Json as String
     */
    public static String GetJsonFromStationId(String Bearer, String StationId){
    
        String url = "https://api.foxtrackr.com/sigfox/stations/3.0/detail/" + StationId + "/5000/";
        String JsonData = computeRequest(url, Method.GET, "Authorization", Bearer, "Accept", "application/json");   
        
        return JsonData;
    }
    
    
    /**
     * Performs Curl request on foxtrackr's api using given bearer and an area with GPS coordinates
     * @param Bearer Token
     * @param minLat Minimum Latitude of the area
     * @param minLng Minimum Longitude of the area
     * @param maxLat Maximum Latitude of the area
     * @param maxLng Maximum Longitude of the area
     * @return Parsed Json as String
     */
    public static String GetJsonFromArea(String Bearer, double minLat, double minLng, double maxLat, double maxLng){

    	String url = "https://api.foxtrackr.com/sigfox/stations/3.0/search/" + minLat + "/" + minLng + "/" + maxLat + "/" + maxLng + "/";
    	String JsonData = computeRequest(url, Method.GET, "Authorization", Bearer);
			
        return JsonData;
    }

    
    /**
     * Performs Curl request on foxtrackr's api using given bearer and the device identifier
     * @param Bearer Token
     * @param DeviceId Device identifier
     * @return Parsed Json as String
     */
    public static String GetLastCommunicationFronDeviceId(String Bearer, String DeviceId) {
    	String url = "https://api.foxtrackr.com/messages/3.0/uplink/" + DeviceId + "/0/0/0/0/0/";
    	String JsonData = computeRequest(url, Method.GET, "Authorization", Bearer);
			
        return JsonData;
    }
    
    
    /**
     * Performs Curl request on foxtrackr's api using given bearer, the device identifier and the communication identifier
     * @param Bearer Token
     * @param DeviceId Device identifier
     * @param ComId Communication identifier
     * @return Parsed Json as String
     */
    public static String GetJsonFromCommunicationId(String Bearer, String DeviceId, String ComId) {
    	String url = "https://api.foxtrackr.com/messages/3.0/uplink/" + DeviceId + "/" + ComId + "/";
    	String JsonData = computeRequest(url, Method.GET, "Authorization", Bearer);
			
        return JsonData;
    }

}
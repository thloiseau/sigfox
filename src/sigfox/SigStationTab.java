package sigfox;

import java.util.List;
import static java.lang.Math.cos;
import org.json.simple.JSONObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * SigStationTab is a class which compute the location of a sigfox object (antenna, device) from communications
 * @author Sarah Hafsi, Thibaut Loiseau
 * @see SigStationPoint
 * @see SigStation
 */
public class SigStationTab {
	/**
	 * tab is a integer array in which the location is computed by a circle/donut drawing method
	 */
	private ArrayList<Integer> tab;
	
	
	/**
	 * id is the identifier of the antenna
	 */
	private String id;
	
	
	/**
	 * x is the horizontal coordinate of the first cell of the array in meter
	 */
	private int x;
	
	
	/**
	 * y is the vertical coordinate of the first cell of the array in meter
	 */
	private int y;
	

	/**
	 * height is the number of vertical cells in the array
	 */
	private int height;
	
	
	/**
	 * width is the number of horizontal cells in the array
	 */
	private int width;
	
	
	/**
	 * size is the dimension of an array cell in meter
	 */
	private int size;
	
	
	/**
	 * antenna is the computed position of the antenna
	 * @see SigStationPoint
	 */
	private SigStationPoint antenna;
	
	
	/**
	 * byPassed indicates if the position is computed with a bypass
	 */
	private boolean byPassed;
	
	
	/**
	 * Constructs a SigStationTab with the new location of the antenna
	 * @param Station antenna for which the location is computed
	 * @param size is the width and height of an array cell in meters
	 */
	public SigStationTab(SigStation Station, int size){
		this.id = Station.getStationId();
		this.byPassed = false;
		if(Station.points.size() > 2) {
			this.size = size;
			this.byPass(Station.points);
			if(this.antenna == null) {
				this.initTab(Station.points);
				this.sortPoints(Station.points);
				this.computeTab(Station.points);
				this.computeAntennaPosition();
				this.computeRadiusError();
			}
			/* Handing if we are computing the location of a device or an antenna */
			if(Station.getRadius() > 0 && (this.antenna.getDistanceInMeters(new SigStationPoint(Station.getLat(),Station.getLng(),0,0)) > Station.getRadius() || this.antenna.getRadius() > Station.getRadius())) {
				this.antenna = new SigStationPoint(Station.getLat(), Station.getLng(), Station.getRadius(), 0);
			}
		} else {
			this.antenna = new SigStationPoint(Station.getLat(), Station.getLng(), Station.getRadius(), 0);
		}
	}	
	
	
	/**
	 * Returns the horizontal position of the first cell of the array 
	 * @return the horizontal position in meter of the first cell of the array
	 */
	public int getX(){
		return this.x;
	}

	
	/**
	 * Returns the vertical position of the first cell of the array
	 * @return the vertical position in meter of the first cell of the array
	 */
	public int getY(){
		return this.y;
	}
	
	
	/**
	 * Returns the number of horizontal cells in the array
	 * @return the width of the array
	 */
	public int getWidth(){
		return this.width;
	}
	
	
	/**
	 * Returns the number of vertical cells in the array 
	 * @return the height the array
	 */
	public int getHeight(){
		return this.height;
	}
	
	
	/**
	 * Returns the dimension of an array cell in meter
	 * @return the dimension of an array cell in meter
	 */
	public int getSize(){
		return this.size;
	}
	
	
	/**
	 * Returns a JSON file containing the new computed position of an antenna 
	 * (stationID, lat, lng, radius, bypassed, lastUpdate)
	 * @return the results in a JSON file 
	 */
	@SuppressWarnings("unchecked")
	public JSONObject getAntennaPositionInJSON() {
		/* Creating the JSON file with the results*/
		JSONObject json = new JSONObject();
		json.put("stationId", this.id);
        json.put("lat", this.antenna.getLat());
		json.put("lng", this.antenna.getLng());
		json.put("radius", this.antenna.getRadius());
		json.put("bypassed", this.byPassed);
		json.put("lastUpdate", System.currentTimeMillis());
		
		return json;
	}
	
	
	/**
	 * Returns the computed position of the antenna
	 * @return the computed position of the antenna
	 */
	public SigStationPoint getAntennaPosition() {
		return this.antenna;
	}
	
	
	/**
	 * Displays an array showing the results (the antenna position) with the array method
	 */
	public void displayTab() {
		int v = 0;
		
		for(int i = 0; i < this.height ; ++i) {
			for(int j = 0; j < this.width ; ++j) {
				v = tab.get(i * this.width + j);
				if(v != 0)
					System.out.format("%4d", v);
				else
					System.out.print("    ");
			}
			System.out.println();
		}
	}
	
	
	/**
	 * Sorts the list of SigStationPoints compared to the RSSI
	 * @param points is a list of SigStationPoint
	 */
	private void byPass(List<SigStationPoint> points) {
		ArrayList<SigStationPoint> closedPoints = new ArrayList<SigStationPoint>();
		
		for(SigStationPoint p : points) {
			if(p.getRSSI() > -81) {
				closedPoints.add(p);
			}
		}
		
		/*if(!closedPoints.isEmpty()){
			points = closedPoints;
			this.byPassed = true;
		}*/
		
		if(!closedPoints.isEmpty()) {
			this.byPassed = true;
			double sumLat = 0;
			double sumLng = 0;
			double rad = 0;
			double lat;
			double lng;
			for(SigStationPoint p : closedPoints) {
				sumLat += p.getLatPositionFromOriginInMeters();
				sumLng += p.getLngPositionFromOriginInMeters();
				rad += p.getRadiusInMetersFromRSSI();
			}
			
			lat = sumLat / (111195.0 * closedPoints.size());
			lng = sumLng / (111195.0 * closedPoints.size() * Math.cos(Math.toRadians(lat)));
			
			this.antenna = new SigStationPoint(lat, lng, Math.floor(rad/closedPoints.size()), 0);
		}
	}
	
	
	/**
	 * Returns the computed antenna position with the array method
	 */
	private void computeAntennaPosition() {
		int max = 0;
		int nb = 0;
		double k = 0;
		double l = 0;
		double lat;
		double lng;
		
		for(int i = 0; i < this.tab.size(); ++i) {
			if(max < tab.get(i))
				max = tab.get(i);
		}
		
		for(int i = 0; i < this.height; ++i) {
			for(int j = 0; j < this.width; ++j) {
				if(this.tab.get(i * this.width + j) == max) {
					nb++;
					k += i;
					l += j;
				}
			}
		}
		
		k /= (double)nb;
		l /= (double)nb;
		
		lat = (this.x - this.size/2 - k * this.size) / 111195.0;
		lng = (this.y + this.size/2 + l * this.size) / (111195.0 * Math.cos(Math.toRadians(lat)));
		
		this.antenna = new SigStationPoint(lat, lng , this.size/2, 0);
	}
	
	
	/**
	 * Initializes an array regarding the list of points considered
	 * @param points is a list of SigStationPoint
	 */
	private void initTab(List<SigStationPoint> points) {
		this.x = maxLatInMeters(points) + this.size/2;
		this.y = minLngInMeters(points) - this.size/2;
		this.height = (int)Math.ceil((this.x - minLatInMeters(points)) / this.size) + 2;
		this.width = (int)Math.ceil((maxLngInMeters(points) - this.y) / this.size) + 2;
		
		this.tab = new ArrayList<Integer>();
		for(int i = 0; i<this.height * this.width ; ++i) {
			this.tab.add(0);
		}
	}
	
	
	/**
	 * Sorts the points in a same cell : keeps the points with the strongest RSSI
	 * @param points is a list of SigStationPoint
	 */
	private void sortPoints(List<SigStationPoint> points) {
		// Suppression des points dans une meme maille : on prend les points dont le RSSI est le plus fort
		int i=0;
		while (i<points.size()) {
			int[] posI = this.getPositionInTab(points.get(i));
			int j=i+1;
			while (j<points.size()) {
				int[] posJ = this.getPositionInTab(points.get(j));
				if (posI[0]-posJ[0] == 0 && posI[1]-posJ[1] == 0) {
					if (points.get(i).getRSSI() < points.get(j).getRSSI()) {
						points.remove(i);
					}else {
						points.remove(j); 
					}
				}
				j++;
			}
			i++;
		}
	}
	
	
	/**
	 * Draws an array modeling the results with the array method
	 * @param points is a list of SigStationPoint
	 */
	private void computeTab(List<SigStationPoint> points) {
		int max = 0;
		
		for(SigStationPoint p : points) {
			/*int d = (int)Math.round(p.getRayon()/this.size);
			if(d>max) {
				max = d;
			}*/

			int d = (int)p.getRSSI();
			if(d<max) {
				max = d;
			}
		}
		
		for(SigStationPoint p : points){
			int weighting = 1 - max + (int)p.getRSSI();
			int[] pos = this.getPositionInTab(p);
			int radius = (int)Math.round(p.getRayon() / this.size);
			HashSet<int[]> pixels = circleAndres(pos[0], pos[1], radius);
			drawCircle(pixels, weighting);
			if(radius > 2) {
				for(int i = 1; i < ((radius - 1)/2); ++i) {
					pixels = circleAndres(pos[0], pos[1], (int)Math.round(p.getRayon() / this.size) + i);
					drawCircle(pixels, weighting);
					pixels = circleAndres(pos[0], pos[1], (int)Math.round(p.getRayon() / this.size) - i);
					drawCircle(pixels, weighting);
				}
		
			}
		}
	}
	
	
	/**
	 * Returns the position of a SigStationPoint in the array, following the array method 
	 * @param p is a SigStationPoint
	 * @return the position of the SigStationPoint p in the array
	 */
	private int[] getPositionInTab(SigStationPoint p){
		int[] position;
		double x = this.x - p.getLatPositionFromOriginInMeters();
		double y = p.getLngPositionFromOriginInMeters() - this.y;
		
		position = new int[2];
		
		position[0] = (int)Math.floor(x / this.size); 
		position[1] = (int)Math.floor(y / this.size);
		
		return( position );
	}

	
	/**
	 * Draws the circles for the array method 
	 * @param pixels
	 * @param weighting
	 */
	private void drawCircle(HashSet<int[]> pixels, int weighting) {
		for(int[] t : pixels) {
			if(t[0] > -1 && t[0] < this.height && t[1] > -1 && t[1] < this.width) {
				//Avec ponderation
				tab.set(t[0] * this.width + t[1], tab.get(t[0] * this.width + t[1]) + weighting);
				//Sans pondaration
				//tab.set(t[0] * this.width + t[1], tab.get(t[0] * this.width + t[1]) + 1);
			}
		}		
	}
	
	
	/**
	 * Andres circle tracing algorithm 
	 * @param x_centre
	 * @param y_centre
	 * @param r
	 * @return
	 */
	private static HashSet<int[]> circleAndres(int x_centre, int y_centre, int r){
	    HashSet<int[]> pixels = new HashSet<int[]>();
	    
	    int x = 0;
	    int y = r;
	    int d = r - 1;
	    
	    if(r==0) {
	    	pixels.add(new int[] {x_centre, y_centre});
	    	return pixels;
		}
	    
	    while(y >= x){	    	
	    	if(x!=y && x!=0) {
		        pixels.add( new int[]{ x_centre + x, y_centre + y });
		        pixels.add( new int[]{ x_centre + x, y_centre - y });
		        pixels.add( new int[]{ x_centre + y, y_centre + x });
		        pixels.add( new int[]{ x_centre - y, y_centre + x });
	        	pixels.add( new int[]{ x_centre - x, y_centre + y });
		        pixels.add( new int[]{ x_centre + y, y_centre - x });
		        pixels.add( new int[]{ x_centre - x, y_centre - y });
		        pixels.add( new int[]{ x_centre - y, y_centre - x });
	        } else {
		    	if(x==y) {
		    		pixels.add( new int[]{ x_centre + x, y_centre + y });
			        pixels.add( new int[]{ x_centre + x, y_centre - y });
			        pixels.add( new int[]{ x_centre - y, y_centre + x });
			        pixels.add( new int[]{ x_centre - y, y_centre - x });
		    	} else {
		    		pixels.add( new int[]{ x_centre + x, y_centre + y });
			        pixels.add( new int[]{ x_centre + x, y_centre - y });
			        pixels.add( new int[]{ x_centre + y, y_centre + x });
			        pixels.add( new int[]{ x_centre - y, y_centre + x });
		    	}
	        }
	        
	        if (d >= 2*x)
	        {
	            d -= 2*x + 1;
	            x ++;
	        }
	        else if (d < 2 * (r-y))
	        {
	            d += 2*y - 1;
	            y --;
	        }
	        else
	        {
	            d += 2*(y - x - 1);
	            y --;
	            x ++;
	        }
	    }
	    
	    return pixels;
	}
    
	
	/**
	 * Returns the smallest latitude value in a SigStationPoint list
	 * @param point is a list of SigStationPoint
	 * @return the smallest latitude
	 */
    private static int minLatInMeters(List<SigStationPoint> point) {
    	int latMin = point.get(0).getLatPositionFromOriginInMeters();
    	int d;
    	
    	for(SigStationPoint p : point) {
    		d = p.getLatPositionFromOriginInMeters() - (int)p.getRayon();
    		if(latMin > d) {
    			latMin = d;
    		}
    	}
    	
    	return latMin;
    }
 
    
    /**
     * Returns the greatest latitude value in a SigStationPoint list
     * @param point is a list of SigStationPoint
     * @return the greatest latitude
     */
    private static int maxLatInMeters(List<SigStationPoint> point) {
    	int latMax = point.get(0).getLatPositionFromOriginInMeters();
    	int d;
    	
    	for(SigStationPoint p : point) {
    		d = p.getLatPositionFromOriginInMeters() + (int)p.getRayon();
    		if(latMax < d) {
    			latMax = d;
    		}
    	}
    	
    	return latMax;
    }

	
    /**
	 * Returns the smallest longitude value in a SigStationPoint list
	 * @param point is a list of SigStationPoint
	 * @return the smallest longitude
	 */
    private static int minLngInMeters(List<SigStationPoint> point) {
    	int lngMin = point.get(0).getLngPositionFromOriginInMeters();
    	int d;
    	
    	for(SigStationPoint p : point) {
    		d = p.getLngPositionFromOriginInMeters() - (int)p.getRayon();
    		if(lngMin > d) {
    			lngMin = d;
    		}
    	}
    	
    	return lngMin;
    }
    
    
    /**
     * Returns the greatest longitude value in a SigStationPoint list
     * @param point is a list of SigStationPoint
     * @return the greatest longitude
     */
    private static int maxLngInMeters(List<SigStationPoint> point) {
    	int lngMax = point.get(0).getLngPositionFromOriginInMeters();
    	int d;
    	
    	for(SigStationPoint p : point) {
    		d = p.getLngPositionFromOriginInMeters() + (int)p.getRayon();
    		if(lngMax < d) {
    			lngMax = d;
    		}
    	}
    	
    	return lngMax;   	
    }
    
    
    /**
     * Computes the radius error with a simple method
     * @param Station Station we want to compute the radius
     * @param size Size of a cell of the array
     */
    private void computeRadiusError() {
    	int radiusError;
    	int i1, i2;
 	   	ArrayList<Integer> temp = new ArrayList<Integer>();
 	   	
 	   	/* Determination des valeurs max du tableau tab */
 	   	temp = this.tab;
 	   	i1 = temp.indexOf(Collections.max(temp));
 	   	temp.remove(temp.get(i1));
 	   	i2 = temp.indexOf(Collections.max(temp)) + 1;
 	   	
 	   	/* Determination des coordonnees en x et y de max1 et max2 en metres */
    	int x_1 = (i1 / this.getWidth()) * size;
    	int y_1 = (i1 % this.getWidth()) * size;
    	
    	int x_2 = (i2 / this.getWidth()) * size;
    	int y_2 = (i2 % this.getWidth()) * size;    	
    	
    	
    	/* Determination de la latitude et longitude, en degres, des deux points */
    	int lat_1 = (int)(x_1 * size / 111195);
    	int lng_1 = (int)(y_1 * size / (111195 * cos(Math.toRadians(lat_1))));

    	int lat_2 = (int)(x_2 * size / 111195);
    	int lng_2 = (int)(y_2 * size / (111195 * cos(Math.toRadians(lat_2))));
    	
    	/* Creation des deux points "max" */
    	SigStationPoint point1 = new SigStationPoint(lat_1,lng_1);
    	SigStationPoint point2 = new SigStationPoint(lat_2,lng_2);
    	
    	/* Distance entre ces deux points */
    	radiusError = point1.getDistanceInMeters(point2);

    	this.antenna.setRadius(radiusError);
    }
 
}
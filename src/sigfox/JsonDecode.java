package sigfox;

import org.json.simple.JSONObject;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

/**
 * JsonDecode is a class that decode data in JSON String
 * @author Sarah Hafsi, Thibaut Loiseau
 * @see SigStation
 */
public class JsonDecode {
	
    /**
     * Decodes JSON String into a given SigStation object
     * @param ToDecode String in JSON within data to decode
     * @param Station Station returned with the data in the JSON
     */
    public static void Decode(String ToDecode, SigStation Station){
       try{
            JSONParser parser = new JSONParser();

            JSONObject obj = (JSONObject) parser.parse(ToDecode);

            Station.setStationId((String)obj.get("stationId"));
            Station.setLat((Double)obj.get("lat"));
            Station.setLng((Double)obj.get("lng"));
            Station.setRadius((Double)obj.get("radius"));

            JSONArray arr = (JSONArray)obj.get("points");
            
            for (int i = 0; i < arr.size(); i++)
            {
                Station.points.add(new SigStationPoint(
                        (Double)((JSONObject)arr.get(i)).get("lat"),
                        (Double)((JSONObject)arr.get(i)).get("lng"),
                        (Double)((JSONObject)arr.get(i)).get("radius"), 
                        (Double)((JSONObject)arr.get(i)).get("rssi")));
            }

        }catch(ParseException pe){

            System.out.println("position: " + pe.getPosition());
            System.out.println(pe);
        }
    }

    
    /**
     * Decodes JSON String into a list of station id
     * @param ToDecode String in JSON within the data to decode
     * @param list List of station ID
	 */
    public static void DecodeArray(String ToDecode, List<String> list){
    	try { 	
	    	JSONParser parser = new JSONParser();
	        JSONArray obj = (JSONArray) parser.parse(ToDecode);
	    	
	    	for(int i = 0; i < obj.size(); ++i) {
	    		if((boolean)((JSONObject)obj.get(i)).get("positionned") == false)
	    			list.add((String)((JSONObject)obj.get(i)).get("stationId"));
	    	}
	    	
    	} catch(ParseException pe) {
	    	System.out.println("position: " + pe.getPosition());
            System.out.println(pe);
	    }
    }

    
    /**
     * Decodes JSON String and gets the communication identifier
     * @param toDecode String in JSON within the data to decode
     * @return the communication identifier
     */
    public static String getComId(String toDecode){
    	String comId = new String();
    	int i = 0;
    	
    	try{
            JSONParser parser = new JSONParser();
            JSONArray obj = (JSONArray) parser.parse(toDecode);
            
            while(!((String)((JSONObject)obj.get(i)).get("frType")).equals("data")) {
            	i++;
            }
            
            comId = (String)((JSONObject)obj.get(i)).get("messageUid");        

        }catch(ParseException pe){

            System.out.println("position: " + pe.getPosition());
            System.out.println(pe);
        }
    	
    	return comId;
    }
    
    
    /**
     * Decodes JSON String into a list of stations and RSSIs
     * @param toDecode String in JSON within the data to decode
     * @param stations List of station id
     * @param rssis List of RSSIs
     */
    public static void decodeCommunicationStationList(String toDecode, List<String> stations, List<Double> rssis) {
    	try{
            JSONParser parser = new JSONParser();

            JSONArray obj = (JSONArray)parser.parse(toDecode);    
            
            for (int i = 0; i < obj.size(); i++)
            {
            	stations.add((String)((JSONObject)obj.get(i)).get("from"));
                rssis.add((Double)((JSONObject)obj.get(i)).get("rssi"));
            }

        }catch(ParseException pe){

            System.out.println("position: " + pe.getPosition());
            System.out.println(pe);
        }
    }

    
    /**
     * Decodes JSON String from a device and the RSSI
     * @param toDecode String in JSON within the data to decode
     * @param device Device returned with the data in the JSON
     * @param rssi RSSI of the communication
     */
    public static void decodeStationCommunicationLocation(String toDecode, SigStation device, double rssi) {
    	try{
            JSONParser parser = new JSONParser();

            JSONObject obj = (JSONObject)parser.parse(toDecode);    
            
            device.points.add(new SigStationPoint(
            		(Double)((JSONObject)obj).get("lat"),
            		(Double)((JSONObject)obj).get("lng"),
            		0, rssi));

        }catch(ParseException pe){

            System.out.println("position: " + pe.getPosition());
            System.out.println(pe);
        }
    }
}
